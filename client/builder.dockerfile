FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ="Europe/London"

RUN apt-get update -y

RUN apt-get install -y \
    wget \
    unzip \
    git \
    curl

RUN apt-get install -y \
    python3 \
    python3-pip

# Install bst and plugins
ARG BST_VERSION=1.93.4-53-g7ff7fb5cde1b491c2e2a321b705d695f48980cfe
ARG BST_PLUGINS_CONTAINER_VERSION=0.4.0
ARG BST_PLUGINS_EXPERIMENTAL_VERSION=1.93.4

RUN git clone https://gitlab.com/BuildStream/buildstream.git && \
  cd buildstream && \
  git checkout "${BST_VERSION}" && \
  pip3 install --user .

RUN pip3 install --user \
  bst-plugins-container=="${BST_PLUGINS_CONTAINER_VERSION}"

RUN git clone https://gitlab.com/BuildStream/bst-plugins-experimental.git && \
  cd bst-plugins-experimental && \
  git checkout "${BST_PLUGINS_EXPERIMENTAL_VERSION}" && \
  pip3 install --user .

# Setuptools is required for loading plugins and is already present in the system
# python. Force reinstall in user python path so it can be coppied accross.
RUN pip3 install --force-reinstall --user setuptools

# Install buildbox
ARG BUILDBOX_VERSION=0.0.7-f938a187
RUN echo bb version: ${BUILDBOX_VERSION}
RUN curl https://buildbox-casd-binaries.nyc3.cdn.digitaloceanspaces.com/buildbox-x86_64-linux-${BUILDBOX_VERSION}.tar.xz > buildbox.tar.xz
RUN tar xvf buildbox.tar.xz -C /root/.local/bin

# Install bazel
ARG BAZEL_VER=3.1.0
RUN wget "https://github.com/bazelbuild/bazel/releases/download/${BAZEL_VER}/bazel-${BAZEL_VER}-installer-linux-x86_64.sh"
RUN chmod +x "bazel-${BAZEL_VER}-installer-linux-x86_64.sh"
RUN "./bazel-${BAZEL_VER}-installer-linux-x86_64.sh" --prefix=/root/.local

RUN apt-get install -y \
    patch \
    ostree \
    lzip
