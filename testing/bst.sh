#!/bin/bash
set -ex
BST_VERSION=1.93.4
BST_PLUGINS_VERSION=1.93.3

mkdir -p testing
cd testing

git clone --branch "${BST_VERSION}" https://gitlab.com/BuildStream/buildstream.git && \
    bst -C buildstream/doc/examples/autotools build hello.bst

git clone --branch "${BST_PLUGINS_VERSION}" https://gitlab.com/BuildStream/bst-plugins-experimental.git && \
    bst -C bst-plugins-experimental/tests/integration/project build bazelize/empty.bst
