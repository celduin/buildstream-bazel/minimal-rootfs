#!/bin/bash
set -ex
# Clone bazel examples repo, there are currently no tags/releases so use fixed
# commit from master which is known to work with default bazel version
BAZEL_EXAMPLES_VERSION=2143d40bb9133e174496de4c20d71b0f5525a901
git clone https://github.com/bazelbuild/examples.git bazel-examples
git -C bazel-examples checkout ${BAZEL_EXAMPLES_VERSION}

cd bazel-examples/cpp-tutorial/stage1

bazel build //main:hello-world
bazel run //main:hello-world
