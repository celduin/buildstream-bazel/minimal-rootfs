
## CI Setup

The CI used for building container images from this project uses dedicated AWS
runners using the Celduin project GitLab bastion.

This GitLab Bastion is currently a hand maintained EC2 instance found at
ec2-3-8-190-239.eu-west-2.compute.amazonaws.com. In order to access it, you'll
need a valid AWS access key for the Celduin AWS account.

## Creating a new runner

To be able to add a custom runner to a GitLab group/project, you will need
sufficient privileges to be able to access the CI settings on that
group/project.

First you will need a token from the group/project you want to use the new
runner. This can be obtained by going to Settings > CI/CD.

When you have this token, ssh into the bastion machine as a sudoer.

Run `sudo gitlab-runner register`. gitlab runner will prompt you for various
bits of information. The gitlab-ci coordinator URL should be:
https://gitlab.com/. The other bits of information are are fairly self
explanatory, one of which will be the token you obtained earlier. Note that any
tags and description you provide in this process can only be changed via
CI-settings in the gitlab project once the runner is started. An example of this
process is shown [here](https://docs.gitlab.com/runner/register/#gnulinux).

Open the gitlab-runner configuration /etc/gitlab-runner/config.toml. You will
see that a new block containing skeleton config for a new runner has been
appended to the pre-existing config. You should alter this to create a runner
suitable for your needs. The actual runner configuration won't be covered here,
but for reference you can use the existing runner configuration and [GitLab's
docs](https://docs.gitlab.com/ee/ci/runner_autoscale_aws/).

Once you have configured your new runner, run `sudo gitlab-runner start` to get
gitlab-runner to pick up the updated configuration. If you go back to the CI
settings of the project (where you got your token) you should see your new runner
listed and available to use.

## Updating runner configuration

To update runner configuration, ssh into the bastion. The runner configuration
can be found in /etc/gitlab-runner/config.toml. Once you have made changes to
this configuration, reload the updated config by doing `sudo gitlab-runner
start`.

Note that runner's tags and description can only be changed via the CI setting
in GitLab.

The gitlab-runner configuration docs can be found [here](https://docs.gitlab.com/runner/configuration/index.html)
